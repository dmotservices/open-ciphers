MACS6118, Cryptography project
Denton Tyndale
=========================================


README
======

Contents:
 - Bash scripts (amsc.sh, asc.sh, freq.sh, masc.sh, purge.sh, rail.sh, upper.sh)
 - Source code (Amsc.java, Asc.java, Freq.java, Masc.java, Purge.java, Rail.java, Upper.java, CryptoMath.java, CryptoCipher.java)
 - Programmer's documentation (index.html)
 - User documentation (README.txt)


USER DOCUMENTATION
==================

1. Install the latest Java Runtime Environment (http://java.sun.com)

2. Download and unzip the project compressed archive

3. In Unix-like systems, use the bash scripts provided.
   Make them executable with "chmod +x nameofscript.sh"

   For any system, use the JRE to execute.
   [$ at the beginning of the line is the command prompt. The output follows]
   Examples:

DISPLAY SYNTAX
   $java Asc
    Performs the Affine cipher.
    syntax: [-k<A-Z>] [-m<A-Z>] [-e] [-d] <input_text>
      [-k<A-Z>] key for rotational offset
      [-m<A-Z>] key for multiplier
      [-e] encrypt using the key
      [-d] decrypt using the key
      <[-Ffilename]/[input_text]> Input file or text to be processed.

ENCRYPT
   $java Asc -kR -mD -e This is the affine cipher
   WMPTPTWMDRGGPEDXPKMDQ

DECRYPT
   $java Asc -kR -mJ -d WMPTPTWMDRGGPEDXPKMDQ
   THISISTHEAFFINECIPHER

FILE OPERATION
   $java Asc -kR -mD -e -Ftextfile.txt
   FDRQDAPTXHCDQDAGYDDRWHEXD


-END-

   