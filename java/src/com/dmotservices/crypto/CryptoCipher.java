package com.dmotservices.crypto;

/**
 * Cryptographic ciphers
 *
 * @author      Denton Tyndale (MACS6118)
 * @version     %I%, %G%
 * @since       1.0
 */
public class CryptoCipher
{
  public static int ALPHABETSIZE=26;	// 26 characters from A-Z
  public static String alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  /**
   * Converts a letter A-Z to a number 0-25<br />
   *
   * @param letter	Letter A-Z to be converted to a number 0-25
   * @return		Numeric mapping of A-Z to 0-25
   */
  public static int azIntValue(Character letter)
  {
    return (int)(Character.toUpperCase(letter.charValue()))-(int)'A';
  }

  /**
   * Encrypts with a monoalphabetic substitution cipher<br />
   *
   * @param plaintext	Plaintext to be encrypted
   * @param key		Encryption key which is a character from A-Z
   * @return		Ciphertext of plaintext encrypted with a monoalphabetic substitution cipher
   */
  public static String encryptMonoalphabetic(String plaintext, Character key)
  {
    String ciphertext=new String("");
    for (int i=0; i<plaintext.length(); ++i)
    {
      if (Character.isLetter(plaintext.charAt(i)))	// Is the character a letter?
      {
	// Yes, the character is a letter
	ciphertext+=alphabet.charAt(
	  CryptoMath.modulus(azIntValue(plaintext.charAt(i))+
	    azIntValue(key),ALPHABETSIZE));
      }
    }

    return ciphertext;
  } // end encryptMonoalphabetic()

  /**
   * Decrypts with a monoalphabetic substitution cipher<br />
   *
   * @param ciphertext	Ciphertext to be decrypted
   * @param key		Decryption key which is a character from A-Z
   * @return		Plaintext of ciphertext decrypted with a monoalphabetic substitution cipher
   */
  public static String decryptMonoalphabetic(String ciphertext, Character key)
  {
    String plaintext=new String("");
    for (int i=0; i<ciphertext.length(); ++i)
    {
      if (Character.isLetter(ciphertext.charAt(i)))
      {
	// Yes, the character is a letter
	plaintext+=alphabet.charAt(
	  CryptoMath.modulus(azIntValue(ciphertext.charAt(i))-
	    azIntValue(key),ALPHABETSIZE));
      }
    }

    return plaintext;
  } // end decryptMonoalphabetic()

  /**
   * Encrypts with the Affine cipher<br />
   *
   * @param plaintext	Plaintext to be encrypted
   * @param key		Encryption key for rotational offset which is a character from A-Z
   * @param multiplier	Encryption key for multiplier which is a character from A-Z
   * @return		Ciphertext of plaintext encrypted with the Affine cipher
   */
  public static String encryptAffine(String plaintext, Character key, Character multiplier)
  {
    String ciphertext=new String("");
    for (int i=0; i<plaintext.length(); ++i)
    {
      if (Character.isLetter(plaintext.charAt(i)))	// Is the character a letter?
      {
	// Yes, the character is a letter
	ciphertext+=alphabet.charAt(
	  CryptoMath.modulus(azIntValue(plaintext.charAt(i))*azIntValue(multiplier)+
	    azIntValue(key),ALPHABETSIZE));
      }
    }

    return ciphertext;
  } // end encryptAffine()

  /**
   * Decrypts with the Affine cipher<br />
   *
   * @param ciphertext	Ciphertext to be decrypted
   * @param key		Decryption key for rotational offset which is a character from A-Z
   * @param multiplier	Decryption key for multiplier which is a character from A-Z
   * @return		Plaintext of ciphertext decrypted with the Affine cipher
   */
  public static String decryptAffine(String ciphertext, Character key, Character multiplier)
  {
    String plaintext=new String("");
    for (int i=0; i<ciphertext.length(); ++i)
    {
      if (Character.isLetter(ciphertext.charAt(i)))
      {
	// Yes, the character is a letter
	plaintext+=alphabet.charAt(
	  CryptoMath.modulus((azIntValue(ciphertext.charAt(i))-azIntValue(key))*azIntValue(multiplier),ALPHABETSIZE));
      }
    }

    return plaintext;
  } // end decryptAffine()

  /**
   * Encrypts with the Rail cipher<br />
   *
   * @param plaintext	Plaintext to be encrypted
   * @param key		The number of rows
   * @return		Ciphertext of plaintext encrypted with the Rail cipher
   */
  public static String encryptRail(String plaintext, int key)
  {
    // W . . . E . . . C . . . R . . . L . . . T . . . E
    // . E . R . D . S . O . E . E . F . E . A . O . C .
    // . . A . . . I . . . V . . . D . . . E . . . N . .
    String ciphertext=new String("");
    String[] ciphertexts=new String[key];		// Rows of ciphertext
    int i,j;
    boolean down=true;
    
    for (i=0; i<key; ++i)
    {
      ciphertexts[i]=new String("");
    }
    
    j=0;	// Which fence we are on
    down=true;	// Where's the next fence?
    for (i=0; i<plaintext.length(); ++i)
    {
      if (Character.isLetter(plaintext.charAt(i)))	// Is the character a letter?
      {
	// Yes, the character is a letter
	ciphertexts[j]+=plaintext.charAt(i);	// Fill in the rows diagonally
      }

      if (down)				// Where's the next fence?
      {
	// Next fence is down
	++j;
      }
      else
      {
	// Next fence is up
	--j;
      }

      if (j<=0 && !down)		// Are we going up but find ourselves on the first fence?
      {
	// Yes
	down=!down;			// Change direction
      }
      else if (j>=key-1 && down)	// Are we going down but find ourselves on the lowest fence?
      {
	// Yes
	down=!down;			// Change direction
      }

    }
    
    for (i=0; i<key; ++i)
    {
      ciphertext+=ciphertexts[i];	// join rows together
    }
    

    return ciphertext;
  } // end encryptRail()

  /**
   * Decrypts with the Rail cipher<br />
   *
   * @param ciphertext	Ciphertext to be decrypted
   * @param key		The number of rows
   * @return		Plaintext of ciphertext decrypted with the Rail cipher
   */
  public static String decryptRail(String ciphertext, int key)
  {
    // W . . . E . . . C . . . R . . . L . . . T . . . E
    // . E . R . D . S . O . E . E . F . E . A . O . C .
    // . . A . . . I . . . V . . . D . . . E . . . N . .

    String plaintext=new String("");
    String[] plaintexts=new String[key];
    String[] fences=new String[key];		// Make pattern fences we can use to decompose the actual ciphertext later
    int[] fenceIterator=new int[key];
    int i,j,k;
    boolean down=true;
    
    for (i=0; i<key; ++i)
    {
      fences[i]=new String("");
      plaintexts[i]=new String("");
      fenceIterator[i]=0;
    }
    
    // SECTION AIM: TO BUILD A PATTERN FENCE SO WE CAN EXTRACT THE LENGTHS OF EACH ROW
    // There's probably a really neat mathematical function to determine this
    // But it didn't occur to me at the time of writing
    j=0;	// Which fence we are on
    down=true;	// Where's the next fence?
    for (i=0; i<ciphertext.length(); ++i)
    {
      fences[j]+="x";	// Fill in the rows diagonally

      if (down)				// Where's the next fence?
      {
	// Next fence is down
	++j;
      }
      else
      {
	// Next fence is up
	--j;
      }

      if (j<=0 && !down)		// Are we going up but find ourselves on the first fence?
      {
	// Yes
	down=!down;			// Change direction
      }
      else if (j>=key-1 && down)	// Are we going down but find ourselves on the lowest fence?
      {
	// Yes
	down=!down;			// Change direction
      }

    }
    
  
    // SECTION AIM: TO BREAK CIPHERTEXT INTO FENCES
    j=0;
    k=ciphertext.length();
    for (i=key-1; i>=0; --i)	// Iterate from last fence to first
    {
      j=fences[i].length();		// Length of the current fence
      k-=j;				// Position in ciphertext where we begin to read a substring as long as the fence
      plaintexts[i]=new String(ciphertext.substring(k,j+k));
    }
     
    // SECTION AIM: TO RECONSTITUTE CIPHERTEXT
    down=true;
    j=0;
    for (i=0; i<ciphertext.length(); ++i)
    {
      plaintext+=plaintexts[j].charAt(fenceIterator[j]);	// Take out the characters diagonally
      ++fenceIterator[j];		// Increment that fence's iterator

      if (down)				// Where's the next fence?
      {
	// Next fence is down
	++j;
      }
      else
      {
	// Next fence is up
	--j;
      }

      if (j<=0 && !down)		// Are we going up but find ourselves on the first fence?
      {
	// Yes
	down=!down;			// Change direction
      }
      else if (j>=key-1 && down)	// Are we going down but find ourselves on the lowest fence?
      {
	// Yes
	down=!down;			// Change direction
      }

    }
 

    return plaintext;
  } // end decryptRail()

  /**
   * Encrypts with an arbitrary monoalphabetic substitution cipher<br />
   *
   * @param plaintext	Plaintext to be encrypted
   * @param keyMap	Encryption key that maps A-Z to an arbitrary alphabet in A-Z one-to-one
   * @return		Ciphertext of plaintext encrypted with the Monoalphabetic substitution cipher
   */
  public static String encryptArbitraryMonoalphabetic(String plaintext, String keyMap)
  {
    String ciphertext=new String("");
    char p;

    if (keyMap.length()==ALPHABETSIZE)
    {
      for (int i=0; i<plaintext.length(); ++i)
      {
	if (Character.isLetter(plaintext.charAt(i)))	// Is the character a letter?
	{
	  // Yes, the character is a letter
	  p=plaintext.charAt(i);
	  ciphertext+=alphabet.charAt(
	    CryptoMath.modulus(azIntValue(keyMap.charAt(azIntValue(p))),ALPHABETSIZE));
	}
      }
    }

    return ciphertext;
  } // end encryptArbitraryMonoalphabetic()

  /**
   * Decrypts with an arbitrary monoalphabetic substitution cipher<br />
   *
   * @param ciphertext	Ciphertext to be decrypted
   * @param keyMap	Decryption key that maps A-Z to an arbitrary alphabet in A-Z one-to-one
   * @return		Plaintext of ciphertext decrypted with the Monoalphabetic substitution cipher
   */
  public static String decryptArbitraryMonoalphabetic(String ciphertext, String keyMap)
  {
    String plaintext=new String("");
    char c;
    int j;

    if (keyMap.length()==ALPHABETSIZE)
    {
      for (int i=0; i<ciphertext.length(); ++i)
      {
	if (Character.isLetter(ciphertext.charAt(i)))
	{
	  // Yes, the character is a letter
	  c=ciphertext.charAt(i);
	  j=keyMap.indexOf(c);
	  if (j>=0)	// was a matching character found
	  {
	    // yes, find the corresponding A-Z character and tack on to plaintext
	    plaintext+=alphabet.charAt(j);
	  }
	}
      }
    }

    return plaintext;
  } // end decryptArbitraryMonoalphabetic()

} // end CryptoCipher