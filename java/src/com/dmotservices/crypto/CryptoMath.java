package com.dmotservices.crypto;

/**
 * Mathematical functions suited for cryptography.
 *
 * @author      Denton Tyndale (MACS6118)
 * @version     %I%, %G%
 * @since       1.0
 */
public class CryptoMath
{
  /**
   * A correct implementation of modulus since the % operator is just remainder and can give a negative result.<br />
   *
   * @param a	value being modded
   * @param b	value being modded
   * @return	a mod b
   */
  public static int modulus(int a, int b)
  {
    return (b+(a%b))%b;
  } // end modulus()

  /**
   * Recursive implementation of greatest common divisor<br />
   *
   * @param a	first integer
   * @param b	second integer
   * @return	gcd(a,b)
   */
  public static int gcd(int a, int b)
  {
    if (b==0)
    {
      return a;
    }
    else
    {
      return gcd(b, modulus(a,b));
    }
  } // end gcd()

  /**
   * Tests if two integers are relatively prime<br />
   *
   * @param a	first integer
   * @param b	second integer
   * @return	<code>true</code> if gcd(a,b)==1
   */
  public static boolean coprime(int a, int b)
  {
    return (gcd(a,b)==1);
  } // end coprime()

} // end CryptoMath