/**
 * Ngram is a command-line program that generates n-gram counts and percent frequencies for input text
 * Three tab-separated entries per line are produced, one line per n-gram.
 * The first entry is the n-gram; the second is the count; the third is the percentage to one decimal place.
 *
 * @author      Denton Tyndale (MACS6118)
 * @version     %I%, %G%
 * @since       1.0
 */

package com.dmotservices.crypto;

import java.io.*;
import java.util.Vector;
import java.util.Collections;

public class Ngram {

	/**
	 * Takes input-text in the command line and generates n-gram counts and
	 * percent frequencies for input text.<br />
	 * Unigrams by default.<br />
	 * Three tab-separated entries per line are produced, one line per n-gram.<br />
	 * 
	 * @param args
	 *            <p>
	 *            Command-line input: [-n[0-9]] [-w] [-p] [-s] [-sf]
	 *            &lt;input_text&gt;<br />
	 *            &lt;-n[0-9]&gt; value of n in n-gram [-w] ignore whitespace<br />
	 *            [-p] ignore punctuation<br />
	 *            [-s] sort in descending order the n-grams and corresponding
	 *            counts and frequencies<br />
	 *            [-sf] sort in descending order by frequencies<br />
	 *            &lt;[-Ffilename]/[input_text]&gt; Input file or text to be
	 *            processed.
	 *            </p>
	 */
	public static void main(String[] args) {
		int ngram = 1; // Assume unigram by default
		int firstWord = 0; // We assume that the first word is at position 0 in
							// the args array
		boolean destroyWhitespace = false; // Do not destroy whitespace by
											// default
		boolean destroyPunctuation = false; // Do not destroy punctuation by
											// default
		boolean sortedOutput = false; // Sorted output
		boolean sortByFrequencies = false; // Sorted by frequencies if
											// sortedOutput is true
		boolean useInputFile = false;
		String inputText = "";

		/**
		 * Entries consisting of symbol and count
		 * 
		 */
		class Entries // Each entry is (symbol, count)
		{

			private Vector<String> symbolTable; // table of symbols for quick
												// reference
			private Vector<Entry> entries; // vector of entries
			private Integer total; // total count of distinct symbols
			private Integer countAllSymbols; // count of all symbols

			/**
			 * An entry consisting of symbol and count
			 * 
			 */
			class Entry // public class = struct
			{
				public String symbol; // the symbol
				public Integer count; // occurrences of the symbol

				/**
				 * Constructor that sets a symbol and the count for an entry
				 * 
				 */
				Entry(String symbol, Integer count) {
					this.symbol = symbol;
					this.count = count;
				} // end constructor
			} // end Entry

			/**
			 * Constructor that sets total to zero and initialises the symbol
			 * table and the entries
			 * 
			 */
			Entries() {
				total = new Integer(0);
				countAllSymbols = new Integer(0);
				symbolTable = new Vector<String>();
				entries = new Vector<Entry>();
			} // end constructor

			/**
			 * Returns a text representation of the entries
			 * 
			 * @return String containing a text representation of the entries.
			 */
			public String toString() {
				String s = new String("");
				Double d;
				Entry e;
				for (int i = 0; i < entries.size(); ++i) {
					e = entries.get(i); // get an entry from the entries table
					// construct a row
					s += e.symbol + "\t";
					s += e.count + "\t";
					d = new Double(new Double((double) e.count
							/ countAllSymbols * 1000.0).intValue()) / 10.0; // Percent
																			// to
																			// 1
																			// decimal
																			// place
					s += d + "\n";
				}

				return s;
			}

			/**
			 * Returns true if a symbol is in the symbol table.
			 * 
			 * @param symbol
			 *            The symbol that is being searched for in the symbol
			 *            table.
			 * @return <code>true</code> if the symbol is found, false
			 *         otherwise.
			 */
			public boolean symbolExists(String symbol) {
				return this.symbolTable.contains(symbol); // Does the symbol
															// exist in the
															// vector?
			} // end symbolExists()

			/**
			 * Returns the count for a symbol
			 * 
			 * @param symbol
			 *            The symbol that is being searched for in the symbol
			 *            table.
			 * @return Count of the number of symbols in the table
			 */
			@SuppressWarnings("unused")
			public Integer countSymbol(String symbol) {
				if (symbolExists(symbol)) // Does the symbol exist in the
											// vector?
				{
					// Yes, the symbol exists in the vector
					Entry entryIterator;
					int count = 0;
					for (int i = 0; i < entries.size(); ++i) // Iterate through
																// entries
					{
						entryIterator = entries.get(i); // Get the ith entry
						count = entryIterator.count; // Get the count of the ith
														// entryIterator
						if (entryIterator.symbol.equals(symbol)) // Have we
																	// found the
																	// symbol?
						{
							// Yes, we have found the symbol
							return new Integer(count); // Return the count of
														// that symbol
						}
					}
				}

				// By default, return no symbol found
				return new Integer(0);
			}

			/**
			 * Adds a symbol to the symbol table
			 * 
			 * @param symbol
			 *            The symbol that is to be added to the symbol table.
			 */
			public void addSymbol(String symbol) {
				if (symbolExists(symbol)) // Does the symbol exist in the
											// vector?
				{
					// Yes, the symbol exists in the vector
					Entry entryIterator;
					int count = 0, searchLen = entries.size(); // Where we will
																// stop
																// iterating
					for (int i = 0; i < searchLen; ++i) // Iterate through
														// entries
					{
						entryIterator = entries.get(i); // Get the ith entry
						count = entryIterator.count; // Get the count of the ith
														// entryIterator
						if (entryIterator.symbol.equals(symbol)) // Have we
																	// found the
																	// symbol?
						{
							// Yes, we have found the symbol
							entries.remove(i); // Remove the ith entry
							--searchLen; // Decrement position where loop ends
											// since we removed an entry
							entries.add(new Entry(symbol, count + 1)); // Add a
																		// new
																		// entry
																		// with
																		// the
																		// new
																		// count
							++total;
						}
					}
				} else {
					// No, the symbol does not exist in the vector

					symbolTable.add(symbol); // Update the symbol table
					entries.add(new Entry(symbol, 1)); // Add symbol to vector
														// of entries and set
														// count to 1
				}
				++countAllSymbols; // Count of all the symbols
			} // end addSymbol()

			/**
			 * Sort the entries in descending order
			 * 
			 */
			public void sort(boolean sortByFrequencies) {
				// nice, good old inefficient bubble sort
				for (int i = 0; i < entries.size(); ++i) {
					for (int j = 0; j < entries.size() - 1; ++j) {
						if (sortByFrequencies) {
							if (entries.get(i).count > entries.get(j).count) {
								Collections.swap(entries, i, j); // swap places
							}
						} else {
							if (entries.get(i).symbol
									.compareTo(entries.get(j).symbol) > 0) {
								Collections.swap(entries, i, j); // swap places
							}
						}
					}
				}
			} // end sort()

		} // end Entries

		if (args.length > 0) // Is there a command-line argument?
		{
			// Yes, there is a command-line argument.

			for (int i = 0; i < args.length; ++i, ++firstWord) // look for
																// options in
																// sequence
			{
				if (args[i].equals("-w")) {
					destroyWhitespace = true; // Ignore whitespace so signal to
												// remove it from input text
				} else if (args[i].equals("-p")) {
					destroyPunctuation = true; // Ignore punctuation so signal
												// to remove it from input text
				} else if (args[i].equals("-s")) {
					sortedOutput = true; // Sort the output in descending order
				} else if (args[i].equals("-sf")) {
					sortedOutput = true; // Sort the output in descending order
					sortByFrequencies = true; // Enable sorting by frequencies
				} else if (args[i].length() > 2
						&& args[i].substring(0, 2).equals("-n")) {
					try {
						ngram = Integer.parseInt(args[i].substring(2,
								args[i].length()));
					} catch (Exception e) {
						System.err.println("error: " + e.getMessage());
					}
				} else if (args[i].length() > 2
						&& args[i].substring(0, 2).equals("-F")) {
					useInputFile = true; // signal that we are getting input
											// from a file
					try {
						FileInputStream fstream = new FileInputStream(
								args[i].substring(2, args[i].length()));
						DataInputStream in = new DataInputStream(fstream);
						BufferedReader br = new BufferedReader(
								new InputStreamReader(in));
						String strLine;

						inputText = new String("");

						// read file line by line
						while ((strLine = br.readLine()) != null) {
							inputText += strLine;
						}
						in.close();
					} catch (Exception e) {
						System.err.println("error: " + e.getMessage());
					}
				} else {
					break; // Exit loop if no more parameters
				}
			}

			if (!useInputFile) // Are we using an input file
			{
				// No, so get input text from command line
				for (int i = firstWord; i < args.length; ++i) {
					inputText += args[i]; // Concatenate words until we get back
											// the original input text
					if (i + 1 < args.length) // Is there a word that follows
												// this word?
					{
						// Yes, there is a word that follows this word
						inputText += " "; // So tack on a space to it
					}
				}
			}

			if (destroyWhitespace) // Do we get rid of whitespace?
			{
				// Yes, get rid of whitespace
				inputText = inputText.replaceAll("\\s", "");
			}

			if (destroyPunctuation) {
				// Yes, get rid of everything that's not alphanumeric or
				// whitespace
				inputText = inputText.replaceAll("[^a-zA-Z0-9\\s]", "");
			}

			Entries freqTable = new Entries();
			for (int i = 0; i < inputText.length() - ngram; ++i) {
				freqTable.addSymbol(inputText.substring(i, i + ngram));
			}

			if (sortedOutput) // Do we sort the output?
			{
				// yes, sort the table of entries
				freqTable.sort(sortByFrequencies);
			}

			System.out.println(freqTable); // Display the table
		} else {
			// No, there is no command-line argument provided so display the
			// syntax
			System.out
					.println("Generates n-gram counts and percent frequencies for input text. Unigrams by default.");
			System.out
					.println("syntax:  [-n[0-9]] [-w] [-p] [-s] [-sf] <input_text>");
			System.out.println("  [-n[0-9]] value of n in n-gram");
			System.out.println("  [-w] ignore whitespace");
			System.out.println("  [-p] ignore punctuation");
			System.out
					.println("  [-s] sort in descending order the n-grams and corresponding counts and frequencies");
			System.out
					.println("  [-sf] sort in descending order by frequencies");
			System.out
					.println("  <[-Ffilename]/[input_text]> Input file or text to be processed");
		}
	} // end main()

} // end Ngram