/**
 * Rail is a command-line program that implements the Rail Fence cipher
 *
 * @author      Denton Tyndale (MACS6118)
 * @version     %I%, %G%
 * @since       1.0
 */

package com.dmotservices.crypto;
import java.io.*;

public class Rail
{

  /**
   * Takes input-text in the command line and performs the Rail Fence cipher.
   *
   * @param args	<p>Command-line input: [-k&lt;A-Z&gt;] [-e] [-d] &lt;input_text&gt;<br />
   *			[-k&lt;num&gt;] key is number of rows<br />
   *			[-e] encrypt using the key<br />
   *			[-d] decrypt using the key<br />
   *			&lt;[-Ffilename]/[input_text]&gt; Input file or text to be processed
   *			</p>
   */
  public static void main(String[] args)
  {
    int firstWord=0;			// We assume that the first word is at position 0 in the args array
    boolean encryptSet=false;		// Neither encryption nor decryption set by default
    boolean decryptSet=false;		// Neither encryption nor decryption set by default
    boolean keySet=false;		// rotational offset key not set by default
    boolean foundError=false;
    boolean useInputFile=false;
    int key = 2;			// default number of rows is 2
    String inputText = "";

    if (args.length>0)			// Is there a command-line argument?
    {
      // Yes, there is a command-line argument.

      for (int i=0; i<args.length; ++i, ++firstWord)	// look for options in sequence
      {
	if (args[i].equals("-e"))
	{
	  encryptSet=true;
	}
	else if (args[i].equals("-d"))
	{
	  decryptSet=true;
	}
	else if (args[i].length()>2 &&	// argument has at least 3 characters
		 args[i].substring(0,2).equals("-k"))	// substring from position 0 to 1 (not 2)
	{
	  keySet=true;
	  try
	  {
	    key=Integer.parseInt(args[i].substring(2,args[i].length()));	// read number after -k
	  }catch (Exception e)
	  {
	    System.err.println("error: "+e.getMessage());
	  }
	  if (key<2)			// Is the key at least 2 rows
	  {
	    // No, the key is not at least two rows
	    System.err.println("error: The key must be at least two rows.");
	    foundError=true;
	  }
	}
	else if (args[i].length()>2 &&
		 args[i].substring(0,2).equals("-F"))
	{
	  useInputFile=true;	// signal that we are getting input from a file
	  try
	  {
	    FileInputStream fstream = new FileInputStream(args[i].substring(2,new String(args[i]).length()));
	    DataInputStream in = new DataInputStream(fstream);
	    BufferedReader br = new BufferedReader(new InputStreamReader(in));
	    String strLine;
	    
	    inputText = new String("");

	    // read file line by line
	    while ((strLine = br.readLine()) != null)
	    {
	      inputText += strLine;
	    }
	    in.close();
	  }catch (Exception e)
	  {
	    System.err.println("error: " + e.getMessage());
	  }
	}
	else
	{
	  break;			// Exit loop if no more parameters
	}
      }

      if (!encryptSet && !decryptSet)
      {
	System.err.println("error: You must choose whether to encrypt or decrypt with -e or -d.");
	foundError=true;
      }

      if (encryptSet && decryptSet)	// Is the user trying to encrypt and decrypt at the same time?
      {
	// Yes, the user is trying to encrypt and decrypt at the same time so flag an error

	System.err.println("error: You may only encrypt or decrypt but not both at the same time.");
	foundError=true;
      }

      if (!keySet)	// Did the user supply a key?
      {
	// No, the user did not supply a key so flag an error

	System.err.println("error: You must supply a key indicating the number of rows.");
	foundError=true;
      }

      if (!foundError)			// Was there an error in the parameters?
      {
	// No, there was not an error

	// No, then get it from the command line
	for (int i=firstWord; i<args.length; ++i)
	{
	  if (useInputFile)		// Are we getting input from a file?
	  {
	    // Yes, we are getting input from a file
	    inputText = new String(inputText.toUpperCase());
	  }
	  else
	  {
	    // No, we are not getting input from a file
	    inputText += new String(args[i]).toUpperCase();	// Concatenate words until we get back the input text in uppercase
	  }
	}


	inputText = new String(inputText.toUpperCase());	// Make input uppercase
	inputText = inputText.replaceAll("\\s","");	// Remove whitespace from input
	inputText = inputText.replaceAll("[^a-zA-Z0-9\\s]",""); // Remove punctuation from input

	String outputText="";
	if (encryptSet)
	{
	  outputText=CryptoCipher.encryptRail(inputText,key);
	}
	
	if (decryptSet)
	{
	  outputText=CryptoCipher.decryptRail(inputText,key);
	}

	System.out.println(outputText);	// Display the ciphertext
      }
    }
    else
    {
      // No, there is no command-line argument provided so display the syntax
      System.out.println("Performs the Rail Fence cipher.");
      System.out.println("syntax: [-k<A-Z>] [-m<A-Z>] [-e] [-d] <input_text>");
      System.out.println("  [-k<num>] key is number of rows");
      System.out.println("  [-e] encrypt using the key");
      System.out.println("  [-d] decrypt using the key");
      System.out.println("  <[-Ffilename]/[input_text]> Input file or text to be processed");
    }
  } // end main()

} // end Rail