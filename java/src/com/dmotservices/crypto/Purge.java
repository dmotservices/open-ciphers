/**
 * Purge is a command-line program that strips all characters that are not in a particular case
 * By default, all characters are stripped but uppercase characters.
 *
 * @author      Denton Tyndale (MACS6118)
 * @version     %I%, %G%
 * @since       1.0
 */

package com.dmotservices.crypto;
import java.io.*;

public class Purge
{

  /**
   * Takes input-text in the command line and strips all characters but uppercase characters by default.
   *
   * @param args	<p>Command-line input: [-l] [-w] [-p] &lt;input_text&gt;<br />
   *			[-l] strip all characters but lowercase characters<br />
   *			[-w] leave whitespace intact<br />
   *			[-p] leave punctuation intact<br />
   *			&lt;[-Ffilename]/[input_text]&gt; Input file or text to be processed.
   *			</p>
   */
  public static void main(String[] args)
  {
    int firstWord=0;			// We assume that the first word is at position 0 in the args array
    boolean destroyWhitespace=true;	// Destroy whitespace by default
    boolean destroyPunctuation=true;	// Destroy punctuation by default
    boolean destroyLowercase=true;	// Destroy lowercase by default
    boolean useInputFile=false;
    String inputText = "";

    if (args.length>0)		// Is there a command-line argument?
    {
      // Yes, there is a command-line argument.

      for (int i=0; i<args.length; ++i, ++firstWord)	// look for options in sequence
      {
	if (args[i].equals("-l"))
	{
	  destroyLowercase=false;	// Leave lowercase alone. Destroy uppercase instead.
	}
	else if (args[i].equals("-w"))
	{
	  destroyWhitespace=false;	// Leave whitespace alone
	}
	else if (args[i].equals("-p"))
	{
	  destroyPunctuation=false;	// Leave punctuation alone
	}
	else if (args[i].length()>2 &&
		 args[i].substring(0,2).equals("-F"))
	{
	  useInputFile=true;	// signal that we are getting input from a file
	  try
	  {
	    FileInputStream fstream = new FileInputStream(args[i].substring(2,new String(args[i]).length()));
	    DataInputStream in = new DataInputStream(fstream);
	    BufferedReader br = new BufferedReader(new InputStreamReader(in));
	    String strLine;
	    
	    inputText = new String("");

	    // read file line by line
	    while ((strLine = br.readLine()) != null)
	    {
	      inputText += strLine;
	    }
	    in.close();
	  }catch (Exception e)
	  {
	    System.err.println("error: " + e.getMessage());
	  }
	}
	else
	{
	  break;			// Exit loop if no more parameters
	}
      }

      if (!useInputFile)		// Are we using an input file?
      {
	// No, then get the words from the command line
	for (int i=firstWord; i<args.length; ++i)
	{
	  inputText += args[i];		// Concatenate words until we get back the original input text
	  if (i+1<args.length)		// Is there a word that follows this word?
	  {
	    // Yes, there is a word that follows this word
	    inputText += " ";		// So tack on a space to it
	  }
	}
      }

      if (destroyLowercase)		// do we get rid of lowercase characters?
      {
	// yes, get rid of lowercase characters
	inputText = inputText.replaceAll("[a-z]","");
      }
      else
      {
	// no, get rid of uppercase characters
	inputText = inputText.replaceAll("[A-Z]","");
      }

      if (destroyWhitespace)		// do we get rid of whitespace?
      {
	// yes, get rid of whitespace
	inputText = inputText.replaceAll("\\s","");
      }

      if (destroyPunctuation)
      {
	// yes, get rid of everything that's not alphanumeric or whitespace
	inputText = inputText.replaceAll("[^a-zA-Z0-9\\s]","");
      }

      System.out.println(inputText);
    }
    else
    {
      // No, there is no command-line argument provided so display the syntax
      System.out.println("Strips all characters that are not in a particular case.");
      System.out.println("By default, all characters are stripped but uppercase characters.");
      System.out.println("syntax: [-l] [-w] [-p] <input_text>");
      System.out.println("  [-l] strip all characters but lowercase characters");
      System.out.println("  [-w] leave whitespace intact");
      System.out.println("  [-p] leave punctuation intact");
      System.out.println("  <[-Ffilename]/[input_text]> Input file or text to be processed");
    }
  } // end main()

} // end Purge