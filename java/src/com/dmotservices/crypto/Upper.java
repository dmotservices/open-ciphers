/**
 * Upper is a command-line program that converts the case of input text.
 * By default, the input text is converted to uppercase.
 *
 * @author      Denton Tyndale (MACS6118)
 * @version     %I%, %G%
 * @since       1.0
 */

package com.dmotservices.crypto;
import java.io.*;

public class Upper
{

  /**
   * Takes input-text in the command line and converts it to uppercase by default.
   *
   * @param args	<p>Command-line input: [-l] &lt;input_text&gt;<br />
   *			[-l] changes the text to lowercase instead of uppercase<br />
   *			&lt;[-Ffilename]/[input_text]&gt; Input file or text to be processed.
   *			</p>
   */
  public static void main(String[] args)
  {
    int firstWord=0;		// We assume that the first word is at position 0 in the args array
    boolean makeUpper=true;	// We convert to uppercase by default
    boolean useInputFile=false;
    String inputText = "";

    if (args.length>0)		// Is there a command-line argument?
    {
      // Yes, there is a command-line argument.

      for (int i=0; i<args.length; ++i, ++firstWord)
      {
	if (args[i].equals("-l"))	// Is the lowercase operation switch set?
	{
	  // Yes, the lowercase switch is set

	  makeUpper=false;	// Change the operation to set lowercase
	}
	else if (args[i].length()>2 &&
		 args[i].substring(0,2).equals("-F"))
	{
	  useInputFile=true;	// signal that we are getting input from a file
	  try
	  {
	    FileInputStream fstream = new FileInputStream(args[i].substring(2,new String(args[i]).length()));
	    DataInputStream in = new DataInputStream(fstream);
	    BufferedReader br = new BufferedReader(new InputStreamReader(in));
	    String strLine;
	    
	    inputText = new String("");

	    // read file line by line
	    while ((strLine = br.readLine()) != null)
	    {
	      inputText += strLine;
	    }
	    in.close();
	  }catch (Exception e)
	  {
	    System.err.println("error: " + e.getMessage());
	  }
	}
	else
	{
	  break;			// Exit loop if no more parameters
	}
      }
      
      if (!useInputFile)		// Are we using an input file?
      {
	// No, so read input text from the command line
	for (int i=firstWord; i<args.length; ++i)
	{
	  // Begin at the position of the first word and stop at the last

	  inputText += args[i];	// Concatenate words until we get back the original input text
	  if (i+1<args.length)	// Is there a word that follows this word?
	  {
	    // Yes, there is a word that follows this word
	    inputText += " ";	// So, tack on a space to it
	  }
	}
      }

      if (makeUpper)		// Do we make the text uppercase?
      {
	// Yes, output uppercase text
	System.out.println(inputText.toUpperCase());
      }
      else
      {
	// No, output lowercase text
	System.out.println(inputText.toLowerCase());
      }
    }
    else
    {
      // No, there is no command-line argument provided so display the syntax
      System.out.println("Converts the case of input text.");
      System.out.println("By default, the input text is converted to uppercase.");
      System.out.println("syntax: [-l] <input_text>");
      System.out.println("  [-l] converts input_text to lowercase if set, otherwise input_text is converted to uppercase");
      System.out.println("  <[-Ffilename]/[input_text]> Input file or text to be processed");
    }
  } // end main()

} // end Upper