/**
 * Masc is a command-line program that performs monoalphabetic substitutions based on a user-supplied key on characters A-Z.
 *
 * @author      Denton Tyndale (MACS6118)
 * @version     %I%, %G%
 * @since       1.0
 */
package com.dmotservices.crypto;

import java.io.*;

public class Masc
{

  /**
   * Takes input-text in the command line and performs monoalphabetic substitutions based on a user-supplied key on characters A-Z.
   *
   * @param args	<p>Command-line input: [-k&lt;A-Z&gt;] [-e] [-d] &lt;input_text&gt;<br />
   *			[-k&lt;A-Z&gt;] key for the substitution<br />
   *			[-e] encrypt using the key<br />
   *			[-d] decrypt using the key<br />
   *			&lt;[-Ffilename]/[input_text]&gt; Input file or text to be processed.
   *			</p>
   */
  public static void main(String[] args)
  {
    int firstWord=0;			// We assume that the first word is at position 0 in the args array
    boolean encryptSet=false;		// Neither encryption nor decryption set by default
    boolean decryptSet=false;		// Neither encryption nor decryption set by default
    boolean keySet=false;		
    boolean foundError=false;
    boolean useInputFile=false;
    Character key = 'A';		// key defaults to 'A'
    String inputText = "";

    if (args.length>0)			// Is there a command-line argument?
    {
      // Yes, there is a command-line argument.

      for (int i=0; i<args.length; ++i, ++firstWord)	// look for options in sequence
      {
	if (args[i].equals("-e"))
	{
	  encryptSet=true;
	}
	else if (args[i].equals("-d"))
	{
	  decryptSet=true;
	}
	else if (args[i].length()>2 &&	// argument has at least 3 characters
		 args[i].substring(0,2).equals("-k"))	// substring from position 0 to 1 (not 2)
	{
	  keySet=true;
	  key=new Character(args[i].toUpperCase().charAt(2));	// character after -k (converted to uppercase)
	  if (!Character.isLetter(key))			// Is the key a letter?
	  {
	    // No, the key is not a letter so flag an error
	    System.err.println("error: The key must be a character from A-Z.");
	    foundError=true;
	  }
	}
	else if (new String(args[i]).length()>2 &&
		 args[i].substring(0,2).equals("-F"))
	{
	  useInputFile=true;	// signal that we are getting input from a file
	  try
	  {
	    FileInputStream fstream = new FileInputStream(args[i].substring(2,new String(args[i]).length()));
	    DataInputStream in = new DataInputStream(fstream);
	    BufferedReader br = new BufferedReader(new InputStreamReader(in));
	    String strLine;
	    
	    inputText = new String("");

	    // read file line by line
	    while ((strLine = br.readLine()) != null)
	    {
	      inputText += strLine;
	    }
	    in.close();
	  }catch (Exception e)
	  {
	    System.err.println("error: " + e.getMessage());
	  }
	}
	else
	{
	  break;			// Exit loop if no more parameters
	}
      }

      if (!encryptSet && !decryptSet)
      {
	System.err.println("error: You must choose whether to encrypt or decrypt with -e or -d.");
	foundError=true;
      }

      if (encryptSet && decryptSet)	// Is the user trying to encrypt and decrypt at the same time?
      {
	// Yes, the user is trying to encrypt and decrypt at the same time so flag an error

	System.err.println("error: You may only encrypt or decrypt but not both at the same time.");
	foundError=true;
      }

      if (!keySet)	// Did the user supply a key?
      {
	// No, the user did not supply a key so flag an error

	System.err.println("error: You must supply a key.");
	foundError=true;
      }

      if (!foundError)			// Was there an error in the parameters?
      {
	// No, there was not an error

	// No, then get it from the command line
	for (int i=firstWord; i<args.length; ++i)
	{
	  if (useInputFile)		// Are we getting input from a file?
	  {
	    // Yes, we are getting input from a file
	    inputText = new String(inputText.toUpperCase());
	  }
	  else
	  {
	    // No, we are not getting input from a file
	    inputText += new String(args[i]).toUpperCase();	// Concatenate words until we get back the input text in uppercase
	  }
	}

	inputText = new String(inputText.toUpperCase());	// Make input uppercase
	inputText = inputText.replaceAll("\\s","");	// Remove whitespace from input
	inputText = inputText.replaceAll("[^a-zA-Z0-9\\s]",""); // Remove punctuation from input


	String outputText="";
	if (encryptSet)
	{
	  outputText=CryptoCipher.encryptMonoalphabetic(inputText,key);
	}
	
	if (decryptSet)
	{
	  outputText=CryptoCipher.decryptMonoalphabetic(inputText,key);
	}

	System.out.println(outputText);	// Display the ciphertext
      }
    }
    else
    {
      // No, there is no command-line argument provided so display the syntax
      System.out.println("Performs monoalphabetic substitutions based on a user-supplied key on characters A-Z.");
      System.out.println("syntax: [-k<A-Z>] [-e] [-d] <input_text>");
      System.out.println("  [-k<A-Z>] key for the substitution");
      System.out.println("  [-e] encrypt using the key");
      System.out.println("  [-d] decrypt using the key");
      System.out.println("  <[-Ffilename]/[input_text]> Input file or text to be processed.");
    }
  } // end main()

} // end Upper